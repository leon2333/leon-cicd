Name:Ang LI
Student ID:20086443
Description:This project is designed for people who like to eat and cook.This website gathers gourmet and delicious food from all over the world. In the first part, you can share the food experience with more users, that is to say, you can see other people's food experience or cooking experience, and you can also write your own food experience articles. In addition, users can comment on their favorite articles. In the second part, users can buy ingredients and foods on this website. This website will sell high-quality food. Through the search function, users can fuzzy search to get relevant product items and browse them. Users can choose their own favorite food and then produce an order for products.
Functions

1../routes/foods -./model/foods- FoodSchema
（get get/:id post put/:id/likes delete/:id）
In this section, the users can see the food recommendation and cooking methods from all over the world.

Website administrators can add, delete, and view foods.Customers can give a like for good foods and see the foods in the food list.

2../routes/products -./model/products-ProductSchema
（get get/:id post put/:id/likes delete/:id）

In this section, users can browse the food mall and buy all kinds of foods and ingredients displayed on the website.
Website administrators can add, delete and view products.Customers can see all the items in the product list and give  a like for them.

3../routes/regist - ./model/register-UserSchema

Registration function, fast registration by user name and password
Here I control and verify the input data. The user name and password entered during registration cannot be empty. The minimum password is 3 and the maximum password is 10 (for security reasons, further change the password length).

4../routes/log - ./model/user-UserSchema
(post)

Log in function, verify whether the input data is registered, compare the input data with the existing data in the database, and judge whether it is a registered user.

5../routes/search-./model/products-ProductSchema
(post)

Fuzzy search function, search a letter, you can search all product names containing this letter, such as search c, you can get the result "cake" and other product names with b bletters

6../routes/userEssay - ./model/essay -EssaySchema
（get get/:id post put/:id/likes delete/:id）

In this module, users can publish, delete and view their own articles. Other users can also give the like and comment for the articles.

7../routes/comment -./model/comment -CommentSchema
(post)
The comment function allows users to comment on articles. Comment and essay are connected by the essay_id . The name and content of the comment stored in comment  ,these attributes will be transmitted to essay list. So,uers can see the comments below the essay.


Git: https://leon66666@bitbucket.org/leon66666/assignment1-foodhub.git

heroku：https://floating-hamlet-95242.herokuapp.com/

DX approach：Automated Testing

Youtobe link：https://youtu.be/LW66vi7-pP8









